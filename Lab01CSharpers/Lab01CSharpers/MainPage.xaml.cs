﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab01CSharpers
{
    public partial class MainPage : ContentPage
    {
        private string wave = "°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸";
        private string koala = "@( * O * )@";
        private string rose = "--------{---(@";
        private string robot = "d[ o_0 ]b";

        private string art = "";
        public string Art
        {
            get
            {
                return art;
            }
            set
            {
                art = value;
                OnPropertyChanged("Art");
            }
        }

        public MainPage()
        {
            BindingContext = this;
            InitializeComponent();
        }

        void OnTapWave(object sender, EventArgs args)
        {
            Art = wave;
        }

        void OnTapKoala(object sender, EventArgs args)
        {
            Art = koala;
        }

        void OnTapRose(object sender, EventArgs args)
        {
            Art = rose;
        }

        void OnTapRobot(object sender, EventArgs args)
        {
            Art = robot;
        }

    }
}
